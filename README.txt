# ImageRegression - diffeomorphic image regression and registration algorithms

This repository holds some new regression and image registration methods that
use vector momenta to do shooting, as well as adjoint equations written entirely
in the Lie algebra (inspired by Lewis&Murray1995).

In addition to geodesic shooting and regression using vector momenta, this
repository also holds an implementation of polynomial regression based on
[Hinkle et al. (ECCV 2012)](http://link.springer.com/chapter/10.1007%2F978-3-642-33712-3_1).
