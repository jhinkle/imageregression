
def Matching(momReg, tdisc, p, optim, integMethod='EULER', updateImage=True, plotEvery=1, outputPrefix=None):
    """
    Match two images using a geodesic and the direct Lie algebra Jacobi equation
    """
    if tdisc[-1].cpg is None:
        raise Exception("Time discretization must include checkpoint at end time")

    # Do first integration.  We integrate at the end of the loop
    (SSEi,KNormi) = EvalForward(tdisc,p,integMethod,updateImage=updateImage)

    # for smoothing momentum a little bit (HACK)
    if mType == MEM_HOST: m0smoother = FluidKernelFFTCPU()
    else: m0smoother = FluidKernelFFTGPU()
    m0smoother.setAlpha(10.)
    m0smoother.setBeta(0)
    m0smoother.setGamma(1.)
    m0smoother.setDivergenceFree(False)
    m0smoother.setGrid(p.I0.grid())

    
    # initialize error variables
    SSE = [SSEi]
    KNorm = [KNormi]

    for it in xrange(1,Niter+1):
        print "Iter %04d/%04d " % (it,Niter),

        # integrate adjoint system
        AdjointIntegration(tdisc,p,integMethod)
        CheckNaN(p.l)

        # avoid introducing crap near the boundaries
        #map(lambda _:Mul_I(_, p.boundarymask), p.l)

        map(lambda l: m0smoother.applyInverseOperator, p.l)
        CheckNaN(p.l)

        # add regularization term, gradient of penalty a|m_0|^2
        #Add_MulC_I(p.l[0],p.m0[0],momReg)
        map(lambda l,m0: Add_MulC_I(l,m0,-2.*momReg), p.l, p.m0)
        CheckNaN(p.l)

        # maxPert: to get step size or set reasonable first step size for adaptive
        if it == 1 and optim.maxPert is not None:
            # TODO: let maxPert be a list
            supnorml1 = LInf(p.l[0])
            if supnorml1*optim.stepSize > optim.maxPert:
                optim.stepSize = optim.maxPert/supnorml1

        # take gradient descent step
        if optim.method == 'FIXEDGD':
            # Go ahead and update momentum
            map(Add_MulC_I,p.m0,p.l,optim.stepSize[0:len(p.m0)])
            CheckNaN(p.m0)

            # Integrate forward and report error
            (SSEi,KNormi) = EvalForward(tdisc,p,integMethod,updateImage=updateImage)
        elif optim.method == 'ARMIJOGD':
            # Perform backtracking (and expansive) line search for epsilon
            # The line search is an important piece of both NCG and LBFGS
            # methods as well.  Both of those should provide faster convergence
            # so this method is more of a testbed.

            # NOTE: this calls EvalForward
            if it < 10:
                (eps,SSEi,KNormi) = ArmijoLineSearch(l1,eps,SSEi+a*KNormi,a,t,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,scratch,l0,integMethod) # reuse l0
            else:
                Add_MulC_I(p.m0[0],l1,-eps)
                (SSEi,KNormi) = EvalForward(t,checkpointstates,checkpointinds,Ninv,residimg,l0,integMethod)

        elif optim.method == 'NCG':
            raise Exception('Non-linear conjugate gradient not yet implemented')
        elif optim.method == 'LBFGS':
            # For this method we approximate the inverse hessian using a lot of
            # rank-one updates.  For the rank-one matrices we actually just save
            # the gradients themselves.  This is the difference between l-BFGS
            # and regular BFGS, which is infeasible in this case because of the
            # huge problem size.  We reset whenever the inner product between
            # the new search direction and the gradient is less than .5 (angle
            # greater than 60 degrees)
            raise Exception('l-BFGS quasi-Newton not yet implemented')
        else:
            raise Exception('Unknown optimization method: '+optim.method)

        # report error after this step
        print "momReg=%f SSE=%f |m0|^2=%f E=%f" % (momReg,SSEi,KNormi,SSEi+momReg*KNormi)

        # save error
        SSE.append(SSEi)
        KNorm.append(KNormi)

        problemiter=10000

        if it == problemiter:
            raw_input("\aShit's about to get real. Press enter and watch the residual")

        if it > problemiter:
            plt.figure('quiver')
            display.Quiver(p.m0[0])
            plt.savefig('quiver'+str(it)+'.png')
            plt.figure('mag')
            SqrMagnitude(p.l0scalartmp,p.m0[0])
            display.DispImage(p.l0scalartmp,newFig=0,rng=[0,1e4])
            plt.savefig('mag'+str(it)+'.png')
            plt.figure('v0')
            p.diffOp.applyInverseOperator(p.v[0],p.m0[0])
            display.Quiver(p.v[0])
            plt.savefig('v0'+str(it)+'.png')


        if plotEvery > 0 and it%plotEvery == 0 or it == Niter-1:
            PlotPolynomial(SSE, map(lambda _:_*momReg, KNorm), tdisc, p, pause=True, outputPrefix=outputPrefix)

        # display the residual image
        #dispImage(residimg,sliceIdx=0)

        # convergence criterion
        #if iter > 30 and abs(SSEi-SSE[-2]) < SSE[0]*1e-8:
            #break

    return (SSE,KNorm)
