from PyCA.Core import *

import PyCA.Common as common
import PyCA.Display as display

import numpy as np
import matplotlib.pyplot as plt
import os, errno

import logging
import sys
import copy
import math
import time

def integrateGeodesic(m0,t,diffOp,\
        keepstates,keepinds,\
        m=None,g=None,ginv=None, scratchInv=None,\
        Ninv=2,integMethod='EULER'):
    """
    Integrate forward to times given by t.  Return the deformation and momentum
    at the last time point
    
    Pass keepinds if you'd like g,ginv,m saved at times other than only the endpoint
    This is useful for regression or for checkpointing
    """
    mType = m0.memType()
    imGrid = m0.grid()

    # allocate scratch memory if necessary
    if m is None: m = Field3D(imGrid, mType)
    if g is None: g = Field3D(imGrid, mType)
    if ginv is None: ginv = Field3D(imGrid, mType)
    if scratchInv is None: scratchInv = Field3D(imGrid, mType)

    # initial conditions 
    SetToIdentity(g)
    SetToIdentity(ginv)

    # do integration (Euler)
    for i in xrange(len(t)-1):
        #sys.stdout.write(',')
        dt = t[i+1]-t[i] # time step

        # check whether we should store this state
        for j in xrange(len(keepinds)):
            if keepinds[j]==i:
                # remember to copy everything
                Copy(keepstates[j][0],g)
                Copy(keepstates[j][1],ginv)

        if integMethod == "EULER":
            # Coadjoint action gives us momentum at time t
            # for scalar momentum, just transport the momentum (with Jacobian) and
            # image (without Jacobian) then do the diamond
            CoAd(m,ginv,m0)

            # Smooth to get velocity at time t
            diffOp.applyInverseOperator(m)

            # Take step for g and ginv
            ComposeVH(g,m,g,dt) # safe to use g for both args
            # Initial estimate for ginv
            ComposeHV(scratchInv,ginv,m,-dt) # cannot use ginv for both args
            # Refine inverse using Ninv fixed point iterations
            #FixedPointInverse(scratchInv,g,Ninv)
            Copy(ginv,scratchInv)
        elif integMethod == "RK4":
            raise Exception('RK4 integration not yet implemented')
        else:
            raise Exception('Unknown integration method: '+integMethod)

    # check if we're keeping last point (hint: we always are)
    for j in xrange(len(keepinds)):
        if keepinds[j]==len(t)-1:
            # remember to copy everything
            Copy(keepstates[j][0],g)
            Copy(keepstates[j][1],ginv)

def AdjointJacobiIntegration(\
        I0, \
        m0,
        t, \
        checkpointstates, \
        checkpointinds, \
        residimgs, \
        residinds, \
        diffOp,
        l0, \
        l1, \
        Ninv=2, \
        integMethod='EULER'):
    mType = residimgs[0].memType()
    imGrid = residimgs[0].grid()

    # we compute l0 on the fly, but we'll sum l1, so zero it first
    SetMem(l1,0.0)

    if len(t)-1 not in checkpointinds:
        raise Exception('Endpoint must be one of the checkpoints passed to AdjointJacobiIntegration')

    # initialize state at endpoint
    endidx = checkpointinds.index(len(t)-1)
    (g,ginv) = checkpointstates[endidx]

    # initialize adjoint variables
    l0scalar = Image3D(imGrid,mType) # the scalar part of l0
    l0scalartmp = Image3D(imGrid,mType) # the scalar part of l0
    gtitj = Field3D(imGrid,mType) # TODO: Be a bit more stingy with memory
    m = Field3D(imGrid,mType)
    dl1 = Field3D(imGrid,mType)
    Kl1 = Field3D(imGrid,mType)
    scratchdl1 = Field3D(imGrid,mType)

    for i in xrange(len(t)-1,1,-1):
        #sys.stdout.write(".")
        dt = t[i]-t[i-1] # positive time-step

        # Possibly better computation for l0
        # l0 is the diamond even if the original momentum wasn't a diamond
        SetMem(l0scalar,0.0)
        for k in xrange(len(residinds)):
            if checkpointinds[residinds[k]] >= i: # this contributes to l0
                # compute deformation to splat back with
                ComposeHH(gtitj,g,checkpointstates[residinds[k]][1])
                # splat
                Splat(l0scalartmp,gtitj,residimgs[k],False)
                # keep the sum
                Add_I(l0scalar,l0scalartmp)
        # compute image I(t_i) and gradient
        ApplyH(l0scalartmp,I0,ginv)
        Gradient(l0,l0scalartmp)

        # NOTE: this is actually negative l0 (following my notation in homoJF.pdf)
        # multiply l0scalar by gradient of image at time t_i
        Mul_I(l0,l0scalar)
        
        # We need v for the infinitesimal parts
        CoAd(m,ginv,m0) # compute momentum

        # this is -sym_v^*l = \ad_v Kl - \ad_{Kl}^*m
        # compute ad* while we have momentum
        diffOp.applyInverseOperator(Kl1,l1) # dl1 is now K(l1)
        CoAdInf(scratchdl1,Kl1,m) # coad part computed as scratchdl1
        # Smooth to get velocity at time t
        diffOp.applyInverseOperator(m) # m is now v
        # overwrite dl1 and start with ad part (positive)
        SetMem(dl1,0)
        AdInf(dl1,m,Kl1) # can't do ad in place, dummy
        # Flat back to momentum
        diffOp.applyOperator(dl1);
        # tack on already-computed ad^* part (negative)
        Sub_I(dl1,scratchdl1)
        #Copy(dl1,scratchdl1)

        # Add l1 (positive since we computed negative l0 to start with)
        Add_I(dl1,l0)

        # Update l1
        # scale l1 by dt for summing
        Add_MulC_I(l1,dl1,-dt) # integrate l1

        # check if next step is a checkpoint
        if i-1 in checkpointinds:
            cp = checkpointstates[checkpointinds.index(i-1)]
            Copy(g,cp[0])
            Copy(ginv,cp[1])
        else:
            # TODO: Work out backward integration compatible with forward
            # i.e. do composeHV instead of composeVH here, etc.

            # otherwise integrate EPdiff to get to next time point
            ComposeVH(g,m,g,-dt) # Take step for g and ginv
            # Initial estimate for ginv
            ComposeHV(scratchdl1,ginv,m,dt)
            # Refine inverse
            #FixedPointInverse(scratchdl1,g,Ninv)
            Copy(ginv,scratchdl1)

def EvalForward(m0, # initial momentum
        t,diffOp,checkpointstates,checkpointinds,Ninv, # integration params
        I0, I1, # input images for computing residual image
        residimg, # residual image (output)
        scratch,integMethod='EULER'): # scratch vector field (for computing KNorm)
    """
    This computes pieces of the objective function, given initial momentum m0.
    This involves forward integration and saving all checkpoints, as well as
    computing the residual image, SSE and |m0|^2.  All these can be reused
    """

    # start by integrating forward
    integrateGeodesic(m0,t,diffOp,checkpointstates,checkpointinds,Ninv=Ninv,integMethod=integMethod)

    # get final state
    endidx = checkpointinds.index(len(t)-1)
    (g1,ginv1) = checkpointstates[endidx]

    # compute residual image
    ApplyH(residimg,I0,ginv1) # deform image
    #ITKFileIO.SaveImage("I01.mhd",residimg)
    Sub_I(residimg,I1) # subtract to get image mismatch

    # compute image error
    SSE = Sum2(residimg)

    # compute norm of momentum
    Copy(scratch,m0)
    diffOp.applyInverseOperator(scratch)
    KNorm = Dot(scratch,m0)

    # checkpointstates and residimg are updated in place
    # return everything that isn't updated in place
    return (SSE,KNorm)

def ArmijoLineSearch(m0, dm0, eps, # current momentum, (negative) search direction, step size
        E0, # Total Energy at m0
        a,  # weighting term for KNorm part of energy
        t, diffOp, checkpointstates, checkpointinds, Ninv, # integration params
        I0, I1, # input images for computing residual
        residimg, # residual image (output)
        scratchm0, scratchKm0, # scratch vector fields
        integMethod='EULER'): # method to use for integration
    """
    Perform expansive then backtracking line search for adaptive step size
    """

    # compute energy if we used double the step size
    Add_MulC(scratchm0,m0,dm0,-2.*eps)
    (SSE,KNorm) = EvalForward(scratchm0,t,diffOp,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,scratchKm0,integMethod)
    E = SSE+a*KNorm

    i = 1
    # Keep doubling until we increase energy
    while (E - E0 < 0.) and (i < 4):
        print "+",
        eps *= 2.
        Add_MulC(scratchm0,m0,dm0,-2.*eps)
        (SSE,KNorm) = EvalForward(scratchm0,t,diffOp,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,scratchKm0,integMethod)
        E = SSE+a*KNorm
        i += 1
    # Now that we have increased, backtrack until we decrease energy
    i = 1
    while (E - E0 > 1e-6) and (i < 50):
        print "-",
        #print "step=%f" % (eps)
        eps *= 0.5
        Add_MulC(scratchm0,m0,dm0,-eps)
        (SSE,KNorm) = EvalForward(scratchm0,t,diffOp,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,scratchKm0,integMethod)
        E = SSE+a*KNorm
        i += 1

    # update m0
    Copy(m0,scratchm0)

    # return the new stepsize (everything else is updated in place)
    return (eps,SSE,KNorm)

def GeodesicMatching(\
        I0, \
        I1, \
        m0, \
        diffOp, \
        a, \
        t, \
        Niter, \
        optMethod, \
        eps, \
        checkpointinds=[], \
        Ninv=2,
        integMethod='EULER'):
    """
    Match two images using a geodesic and the direct Lie algebra Jacobi equation
    """
    mType = I0.memType()
    imGrid = I0.grid()

    if (len(t)-1) not in checkpointinds: checkpointinds.append(len(t)-1)

    endidx = checkpointinds.index(len(t)-1) # index of endpoint

    # allocate memory
    tmp = Image3D(imGrid,mType) # residual vector (scratch)
    residimg = Image3D(imGrid,mType) # residual vector (scratch)
    checkpointstates = [(Field3D(imGrid,mType),Field3D(imGrid,mType)) for idx in checkpointinds]
    # intermediate stuff like gradients and scratch variables
    l0 = Field3D(imGrid,mType) # this is used as a scratch variable later
    l1 = Field3D(imGrid,mType)
    scratch = Field3D(imGrid,mType)

    # Do first integration.  We integrate at the end of the loop
    (SSEi,KNormi) = EvalForward(m0,t,diffOp,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,l0,integMethod)

    # initialize error variables
    SSE = [SSEi]
    KNorm = [KNormi]

    for iter in xrange(1,Niter+1):
        print "Iter %04d/%04d " % (iter,Niter),

        # integrate adjoint system
        AdjointJacobiIntegration(I0, m0, t, checkpointstates, checkpointinds, [residimg], [endidx], diffOp, l0, l1, Ninv,integMethod)

        # add regularization term, gradient of penalty a|m_0|^2
        Add_MulC_I(l1,m0,a)

        # maxPert: to get step size or set reasonable first step size for adaptive
        #if iter == 1:
            #maxPert = 0.1 # maximum length of an update vector
            #supnorml1 = LInf(l1)
            #if supnorml1*eps > maxPert:
                #eps = maxPert/supnorml1

        # take gradient descent step
        if optMethod == 'FIXEDGD':
            # Go ahead and update momentum
            Add_MulC_I(m0,l1,-eps)

            # Integrate forward and report error
            (SSEi,KNormi) = EvalForward(m0,t,diffOp,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,l0,integMethod)
        elif optMethod == 'ARMIJOGD':
            # Perform backtracking (and expansive) line search for epsilon
            # The line search is an important piece of both NCG and LBFGS
            # methods as well.  Both of those should provide faster convergence
            # so this method is more of a testbed.

            # NOTE: this calls EvalForward
            (eps,SSEi,KNormi) = ArmijoLineSearch(m0,l1,eps,SSEi+a*KNormi,a,t,diffOp,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,scratch,l0,integMethod) # reuse l0
        elif optMethod == 'NCG':
            raise Exception('Non-linear conjugate gradient not yet implemented')
        elif optMethod == 'LBFGS':
            # For this method we approximate the inverse hessian using a lot of
            # rank-one updates.  For the rank-one matrices we actually just save
            # the gradients themselves.  This is the difference between l-BFGS
            # and regular BFGS, which is infeasible in this case because of the
            # huge problem size.  We reset whenever the inner product between
            # the new search direction and the gradient is less than .5 (angle
            # greater than 60 degrees)
            raise Exception('l-BFGS quasi-Newton not yet implemented')
        else:
            raise Exception('Unknown optimization method: '+optMethod)

        # report error after this step
        print "a=%f SSE=%f |m0|^2=%f E=%f" % (a,SSEi,KNormi,SSEi+a*KNormi)

        if iter == 540:
            raw_input("\aShit's about to get real. Press enter and watch the residual")

        if iter > 540:
            plt.figure('quiver')
            display.Quiver(m0)
            plt.savefig('quiver'+str(iter)+'.png')
            plt.figure('resid')
            display.DispImage(residimg,newFig=0)
            plt.savefig('resid'+str(iter)+'.png')
            plt.figure('mag')
            SqrMagnitude(tmp,m0)
            display.DispImage(tmp,newFig=0,rng=[0,1e4])
            plt.savefig('mag'+str(iter)+'.png')

        # save error
        SSE.append(SSEi)
        KNorm.append(KNormi)

        #if iter > 30 and abs(SSEi-SSE[-2]) < SSE[0]*1e-8:
            #break

    endidx = checkpointinds.index(len(t)-1)
    (g1,ginv1) = checkpointstates[endidx]
    ApplyH(residimg,I0,ginv1) # deform image and return as residimg

    return (residimg,SSE,KNorm)

def RunScaleWarpIndep(\
        I0, \
        I1, \
        s, \
        a, \
        t, \
        Ninv, \
        checkpointinds, \
        Niter, \
        optMethod, \
        eps, \
        savePrefix = '', \
        disp = True, \
        dispEvery = 500, \
        debug = False, \
        saveData = False, \
        saveScaleData = False, \
        saveEnergyImages = False):
    """
    Run multiscale warping at a set (s) of scales with no influence from
    neighboring scales
    """

    plt.close('all')

    mType = I0.memType()
    imGrid = I0.grid()
    imSz = I0.size()

    # set up all the for all the scales
    if mType == MEM_HOST: diffOp = FluidKernelFFTCPU()
    else: diffOp = FluidKernelFFTGPU()

    # default params
    diffOp.setAlpha(0)
    diffOp.setBeta(0)
    diffOp.setGamma(1)
    diffOp.setGrid(imGrid)

    # initial momentum (output)
    m0 = [Field3D(imGrid,mType) for i in s]

    # Run for first scale
    print "Matching at scale %f" % (s[0])
    diffOp.setAlpha(s[0])
    m0first = Field3D(imGrid,mType)
    SetMem(m0[0],0)
    (I0def,SSEi,KNormi) = GeodesicMatching(I0,I1,m0[0],diffOp,a[0],t,Niter,optMethod,eps,checkpointinds,Ninv)
    I0def.toType(MEM_HOST) # download to free up space on the GPU

    I0defVol = [I0def.tolist()] # this is a 3D volume (list of lists) representing slices that
    SSE = [SSEi]
    KNorm = [KNormi]
    #are deformed images at each scale
    for i in xrange(1,len(s)):
        print "Matching at scale %f (%d of %d)" % (s[i], i+1, len(s))

        # Update parameters
        diffOp.setAlpha(s[i])

        #Copy(m0[i], m0[i-1]) # use previous momentum as initial estimate

        #SetMem(m0[i],0) # start at zero

        # use a scaled version of the previous momentum
        Copy(m0[i], m0[i-1])
        MulC_I(m0[i],.1)

        (I0def,SSEi,KNormi) = GeodesicMatching(I0,I1,m0[i],diffOp,a[i],t,Niter,optMethod,eps,checkpointinds,Ninv)

        # when we use 2D images, let's save all the images as slices of a 3D volume
        I0defVol.append(I0def.tolist())

        # save SSE
        SSE.append(SSEi)
        KNorm.append(KNormi)

    return (I0defVol,SSE,KNorm)

# safe, silent mkdir
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST:
            pass
        else: raise

def Ellipse(N,a,b,mType,blurSigma=0):
    I = np.zeros([N,N])

    for x in xrange(N):
        for y in xrange(N):
            xw = x - N/2.
            yw = y - N/2.
            X = xw/a
            Y = yw/b
            if (X*X + Y*Y) < 1.:
                I[x,y] = 1.

    I = common.GaussianBlur(I.T,blurSigma)

    return common.ImFromNPArr(I,mType=mType)

if __name__ == '__main__':
    #mType = MEM_HOST
    mType = MEM_DEVICE

    prefix = 'testIndep2'
    
    if mType == MEM_HOST: prefix = prefix + 'HOST'
    else: prefix = prefix + 'GPU'

    # make output directory
    mkdir_p(prefix)

    # load base and target image
    #I0 = common.LoadITKImage('testData/ImageSlice0.mhd',mType) # slices
    #I1 = common.LoadITKImage('testData/ImageSlice4.mhd',mType)
    #I0 = common.LoadITKImage('testData/Image0.mhd',mType) # volumes
    #I1 = common.LoadITKImage('testData/Image4.mhd',mType)

    # circle to ellipse
    N = 256
    r = N/4
    a = N*.4
    b = N*.16
    c = N*.20
    d = N*.32
    sc = 0.8 # scale so we don' tget close to the boundary
    r *= sc
    a *= sc
    b *= sc
    c *= sc
    d *= sc
    #a = 36
    #b = 36
    imageSigma = N/32.
    I0l = Ellipse(N,r,r,mType,imageSigma)
    I1l = Ellipse(N,d,c,mType,imageSigma)
    I2l = Ellipse(N,b,a,mType,imageSigma)
    #J = [I0l, I1l, I2l]
    #Jind = [0,29,39]
    #Jt = [0.0,.85,1.0]
    J = [I0l, I2l]
    Jt = [0., 1.]

    # set up time discretization
    Nt = 100
    t = [x*1./Nt for x in range(Nt)]
    Ninv = 0 # number of fixed point iterations to ensure a good inverse is computed

    t = sorted(list(set(t+Jt)))
    Jind = map(t.index, Jt)


    # set up checkpointing for improved adjoint integration (you pay in memory)
    # It's probably a good idea to use 2 or 4 checkpoints at least..
    #checkpointinds = [] # no checkpointing
    checkpointinds = range(len(t)) # all checkpointing: no backward integration of g

    # set up scales
    # this should be a fine discretization when using interdependent scales
    #s = [1e5, 1e4, 1e3, 1e2, 1e1, 1]
    s = [1e5] # translation
    # start getting deformation around s=24000
    s = [1e4] # oversmooth
    s = [1e3] # pretty decent matching,maybe a little over smooth
    #s = [1e2] # trouble converging but match looks good. ??? stepsize issue maybe
    #s = [1e1] # trouble converging but match looks good. ???
    #s = [2**(0.01*i) for i in range(1500,0,-1)] # bunch of scales!

    # set up regularization part
    a = [0.0000 for si in s] # E = a|m0|^2 + |I_0(1)-I_1|^2

    # Optimization parameters
    Niter = 20000
    eps = 50 # don't worry, maxpert takes care of this for the most part
    optMethod = 'FIXEDGD'  # fixed stepsize gradient descent
    #optMethod = 'ARMIJOGD' # gradient descent with backtracking line search
    #optMethod = 'NCG'      # non-linear conjugate gradient (with Armijo)
    #optMethod = 'LBFGS'    # l-BFGS quasi-Newton (with Armijo)

    (I0defVol,SSE,KNorm) = RunScaleWarpIndep(\
                            J[0], \
                            J[1], \
                            s, \
                            a, \
                            t, \
                            Ninv, \
                            checkpointinds, \
                            Niter, \
                            optMethod, \
                            eps, \
                            prefix)
                        
    ## plot SSE as a surface plot
    #fig = plt.figure(1)
    #plt.clf()
    #ax = fig.gca(projection='3d')
    ##S,N = np.meshgrid(range(Niter),scipy.log(s))
    #(S,N) = np.meshgrid(np.arange(Niter),np.arange(len(s)))
    #SSEa = np.array(SSE)
    #ax.plot_surface(S,N,SSEa)

    print "Saving volume of deformed images"

    # output warped image as 3D nrrd
    imSz = I0.Size()
    if (I0.Size().z == 1):
        IoutGrid = I0.grid() # copy original grid but add some depth
        IoutGrid.setSize(Vec3Di(imSz.x, imSz.y, len(s)))
        Is = Image3D(IoutGrid, MEM_HOST)
        Il = np.squeeze(I0defVol).tolist()

        if len(s) == 1: Is.fromlist([Il])
        else: Is.fromlist(Il)

        ITKFileIO.SaveImage(prefix+"/I0defs.mhd",Is)
        # output diff of first with last
        Il -= I0.tolist()
        Is.fromlist(Il)
        ITKFileIO.SaveImage(prefix+"/I0diffs.mhd",Is)
    else:
        # Save out each volume individually
        for i in xrange(len(s)):
            ITKFileIO.SaveImage(prefix+"/I0defScale"+str(s[i])+".mhd",I0defVol[i])
