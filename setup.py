#!/usr/bin/env python2
"""Setup script for ImageRegression package"""

from distutils.core import setup

with open('README.txt') as file:
    # load the description from README so that bitbucket and PyPI pages match
    long_description = file.read()

    setup(name='ImageRegression',
        version='0.01',
        description='Diffeomorphic geodesic and polynomial regression on images',
        author='Jacob Hinkle',
        author_email='jacob@sci.utah.edu',
        url='http://bitbucket.org/scicompanat/imageregression',
        packages=['ImageRegression'],
        long_description=long_description)
