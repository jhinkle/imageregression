
# safe, silent mkdir
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST:
            pass
        else: raise

def Ellipse(N,a,b,mType,blurSigma=0):
    I = np.zeros([N,N])

    for x in xrange(N):
        for y in xrange(N):
            xw = x - N/2.
            yw = y - N/2.
            X = xw/a
            Y = yw/b
            if (X*X + Y*Y) < 1.:
                I[x,y] = 1.

    I = common.GaussianBlur(I.T,blurSigma)

    return common.ImFromNPArr(I,mType=mType)

def makeC(N,r1,r2,t1,t2,blurSigma=0.):
    # we'll populate a numpy array first
    Il = np.zeros([N,N])

    for x in xrange(N):
        for y in xrange(N):
            xw = (x - N/2.)/float(N)
            yw = (y - N/2.)/float(N)
            # get radius
            r = math.sqrt(xw*xw+yw*yw)
            # get theta
            t = math.atan2(yw,-xw)
            if (t1 <= t <= t2) and (r1 <= r <= r2):
                Il[x,y] = 1.

    # blur
    Il = common.GaussianBlur(Il.T,blurSigma)

    # convert to Image3D
    return common.ImFromNPArr(Il,mType=mType)

if __name__ is '__main__':

    # Upload to device if requested
    I.toType(mType)


    #mType = MEM_HOST
    mType = MEM_DEVICE

    prefix = 'testIndep2'
    
    if mType == MEM_HOST: prefix = prefix + 'HOST'
    else: prefix = prefix + 'GPU'

    # make output directory
    mkdir_p(prefix)

    ## load base and target image
    #I0l = common.LoadITKImage('testData/ImageSlice0.mhd',mType) # slices
    #I2l = common.LoadITKImage('testData/ImageSlice2.mhd',mType)
    #I4l = common.LoadITKImage('testData/ImageSlice4.mhd',mType)
    ##I0 = common.LoadITKImage('testData/Image0.mhd',mType) # volumes
    ##I1 = common.LoadITKImage('testData/Image4.mhd',mType)
    #J = [I0l, I2l, I4l]
    #Jind = [0,19,39]

    # set up time discretization
    Nt = 100
    t = [(x)*1./(Nt-1.) for x in range(Nt)]
    #Ninv = 0 # number of fixed point iterations to ensure a good inverse is computed

    # circle to ellipse
    N = 128
    ds = 0.8
    r = N*.2*ds
    a = N*.3*ds
    b = N*.2*ds
    c = N*.37*ds
    d = N*.15*ds
    #a = 36
    #b = 36
    imageSigma = N/38
    I0l = Ellipse(N,r,r,mType,imageSigma)
    I1l = Ellipse(N,a,b,mType,imageSigma)
    I2l = Ellipse(N,c,d,mType,imageSigma)
    #J = [I0l, I1l, I2l]
    #Jt = [0.0,.85,1.0]
    J=[I0l,I1l,I2l]
    Jt =[0,0.5,1.]
    
    ## C data
    ## size of image and C
    #N = 256
    #r1 = .125
    #r2 = .3
    #t1 = -math.pi*.75  # start angle
    #t2 = 0             # end angle for I0
    #t3 = math.pi*.5   # end angle for I1
    ## sigma for smoothing images
    #imageSigma = N/32.
    #Nd = 2 # number of data points
    #Jt = [(i)/(Nd-1.) for i in xrange(Nd)]
    ## Generate data
    #J = [makeC(N,r1,r2,t1,t2+(t3-t2)*tt,blurSigma=imageSigma) for tt in Jt]

    t = sorted(list(set(t+Jt)))
    Jind = map(t.index, Jt)

    # set up checkpointing for improved adjoint integration (you pay in memory)
    # It's probably a good idea to use 2 or 4 checkpoints at least..
    #checkpointinds = [] # no checkpointing except at datapoints
    checkpointinds = range(len(t)) # all checkpointing: no backward integration of g
    cpMemType = None # use same type as state TODO: fix streaming
    #cpMemType = MEM_HOST

    alpha = 1e2
    beta = 0
    gamma = 1
    incomp = False

    k=1 # polynomial order

    maxPert = None # no maxPert
    #maxPert = 1*gamma

    # set up regularization part
    momReg = 0.00005

    # Optimization parameters
    Niter = 2000
    # stepsizes for velocity, acceleration, jerk, snap, crackle, pop, ...
    eps = [20., 50.0]
    optMethod = 'FIXEDGD'  # fixed stepsize gradient descent
    #optMethod = 'ARMIJOGD' # gradient descent with backtracking line search
    #optMethod = 'NCG'      # non-linear conjugate gradient (with Armijo)
    #optMethod = 'LBFGS'    # l-BFGS quasi-Newton (with Armijo)

    #integMethod = 'EULER'
    integMethod = 'RK4'

    optcfg = OptimizationConfig(method=optMethod, Niter=Niter, stepSize=eps, maxPert=maxPert)

    m0all = []
    I0all = []
    for ki in xrange(1,k+1):
        # start with geodesic regression and go up from there to final k

        p = State(J[0], alpha,beta,gamma,incomp, ki, integMethod=integMethod)
        tdisc = SetupTimeDiscretization(t,J,Jind,checkpointinds, ki, cpMemType=cpMemType)
        map(lambda _:SetMem(_,0), p.m0)
        if ki==1: # initial momentum (output)
            # initial image
            SetMem(p.I0, 0)
            for Ji in J: Add_I(p.I0,Ji)
            DivC_I(p.I0, N)
            pass
        else:
            map(Copy, p.m0[0:ki-1], m0all[ki-2])
            Copy(p.I0, I0all[ki-2])

        (SSEi,KNormi) = Matching(momReg,tdisc,p,optcfg,updateImage=True,plotEvery=5000,outputPrefix='ImageRegressionK'+str(ki))
        
        # hold on to all the initial momenta and base images
        m0i = [Field3D(J[0].grid(), MEM_HOST) for i in xrange(ki)]
        I0i = Image3D(J[0].grid(), MEM_HOST)
        map(Copy, m0i, p.m0)
        Copy(I0i, p.I0)
        m0all.append(m0i)
        I0all.append(I0i)
