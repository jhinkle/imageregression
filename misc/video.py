# Utilities for creating a video from ITK-supported 3D images

def makeVideo(fname, I, intensityWindow, framerate):
    """
    Create a video from a stack of slices represented as an Image3D
    """

