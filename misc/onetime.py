
# This is an implementation of "dark matter shooting", in which the evolution of
# the deformation is determined by a time-varying metric.  The time-dependence
# of the metric determines how quickly the scales are refined, as well as how
# fast the deformation slows down and stops.  We use the following metric
# L(t) = t - \nabla^2
# which provides, for instance, 1/t convergence to the limit for an input which
# is a constant momentum m_0
# - JDH 2012

def noop(*args,**kwargs):
    """
    A do-nothing function
    """
    pass

def diffOpLaplacianPlusIdentity(t,imGrid,diffOp):
    """
    L(t) = t - nabla^2
    """
    diffOpParam = DiffOperParam()
    diffOpParam.SetAlpha(1)
    diffOpParam.SetBeta(0)
    diffOpParam.SetGamma(t)

    diffOp.UpdateScaleParams(imGrid, diffOpParam)

def integrateDiffeomorphism(m0,t,diffOp,\
        keepstates,keepinds,\
        diffOpFn=noop,\
        m=None,g=None,ginv=None, scratchInv=None,\
        Ninv=2,integMethod='EULER'):
    """
    This is exactly like integrateGeodesic, except now we use a function
    diffOpFn, which, given a time t, updates the parameters of diffOp

    Note that this is precisely geodesic evolution if you pass in diffOpFn=noop
    """
    mType = m0.memType()
    imGrid = m0.grid()

    # allocate scratch memory if necessary
    if m is None:
        m = Field3D(imGrid, mType)
    if g is None:
        g = Field3D(imGrid, mType)
    if ginv is None:
        ginv = Field3D(imGrid, mType)
    if scratchInv is None:
        scratchInv = Field3D(imGrid, mType)

    # initial conditions 
    HFOpers.setToIdentity(g)
    HFOpers.setToIdentity(ginv)

    # do integration (Euler)
    for i in xrange(len(t)-1):
        #sys.stdout.write(',')
        dt = t[i+1]-t[i] # time step

        # get the metric for this time point (will adjust later if doing RK4)
        diffOpFn(t[i],imGrid,diffOp)

        # check whether we should store this state
        for j in xrange(len(keepinds)):
            if keepinds[j]==i:
                # remember to copy everything
                FOpers.Copy(keepstates[j][0],g)
                FOpers.Copy(keepstates[j][1],ginv)

        if integMethod == "EULER":
            # Coadjoint action gives us momentum at time t
            # for scalar momentum, just transport the momentum (with Jacobian) and
            # image (without Jacobian) then do the diamond
            FOpers.CoAd(m,ginv,m0)

            # Smooth to get velocity at time t
            diffOp.ApplyInverseOperator(m)

            # Take step for g and ginv
            FOpers.composeVH(g,m,g,dt) # safe to use g for both args
            # Initial estimate for ginv
            FOpers.composeHV(scratchInv,ginv,m,-dt) # cannot use ginv for both args
            # Refine inverse using Ninv fixed point iterations
            #FOpers.FixedPointInverse(scratchInv,g,Ninv)
            FOpers.Copy(ginv,scratchInv)
        elif integMethod == "RK4":
            raise Exception('RK4 integration not yet implemented')
        else:
            raise Exception('Unknown integration method: '+integMethod)

    # check if we're keeping last point (hint: we always are)
    for j in xrange(len(keepinds)):
        if keepinds[j]==len(t)-1:
            # remember to copy everything
            FOpers.Copy(keepstates[j][0],g)
            FOpers.Copy(keepstates[j][1],ginv)

def integrateAdjointEquations(\
        I0, \
        m0,
        t, \
        checkpointstates, \
        checkpointinds, \
        residimgs, \
        residinds, \
        diffOp,
        l0, \
        l1, \
        Ninv=2, \
        diffOpFn = noop,\
        diffOpPrimeFn = noop,\
        integMethod='EULER'):
    """
    Do geodesic or scale-based matching.  You must pass in diffOpFn and
    diffOpPrimeFn.  Passing noop for each gives geodesic adjoint equations
    """
    mType = residimgs[0].memType()
    imGrid = residimgs[0].grid()

    # we compute l0 on the fly, but we'll sum l1, so zero it first
    FOpers.SetMem(l1,0.0)

    if len(t)-1 not in checkpointinds:
        raise Exception('Endpoint must be one of the checkpoints passed to AdjointJacobiIntegration')

    # initialize state at endpoint
    endidx = checkpointinds.index(len(t)-1)
    (g,ginv) = checkpointstates[endidx]

    # initialize adjoint variables
    l0scalar = Image3D(imGrid,mType) # the scalar part of l0
    l0scalartmp = Image3D(imGrid,mType) # the scalar part of l0
    gtitj = Field3D(imGrid,mType) # TODO: Be a bit more stingy with memory
    m = Field3D(imGrid,mType)
    dl1 = Field3D(imGrid,mType)
    Kl1 = Field3D(imGrid,mType)
    scratchdl1 = Field3D(imGrid,mType)

    for i in xrange(len(t)-1,1,-1):
        #sys.stdout.write(".")
        dt = t[i]-t[i-1] # positive time-step

        # get the metric for this time point (will adjust later if doing RK4)
        diffOpFn(t[i],imGrid,diffOp)

        # Possibly better computation for l0
        # l0 is the diamond even if the original momentum wasn't a diamond
        IOpers.SetMem(l0scalar,0.0)
        for k in xrange(len(residinds)):
            if checkpointinds[residinds[k]] >= i: # this contributes to l0
                # compute deformation to splat back with
                FOpers.applyH(gtitj,g,checkpointstates[residinds[k]][1])
                # splat
                IFOpers.forwardApply(l0scalartmp,gtitj,residimgs[k],False)
                # keep the sum
                IOpers.Add_I(l0scalar,l0scalartmp)
        # compute image I(t_i) and gradient
        IFOpers.applyH(l0scalartmp,I0,ginv)
        IFOpers.Gradient(l0,l0scalartmp)

        # NOTE: this is actually negative l0 (following my notation in homoJF.pdf)
        # multiply l0scalar by gradient of image at time t_i
        IFOpers.Mul_I(l0,l0scalar)
        
        # We need v for the infinitesimal parts
        FOpers.CoAd(m,ginv,m0) # compute momentum

        # this is -sym_v^*l = \ad_v Kl - \ad_{Kl}^*m
        # compute ad* while we have momentum
        FOpers.Copy(Kl1,l1)
        diffOp.ApplyInverseOperator(Kl1) # dl1 is now K(l1)
        FOpers.coad(scratchdl1,Kl1,m) # coad part computed as scratchdl1
        # Smooth to get velocity at time t
        diffOp.ApplyInverseOperator(m) # m is now v
        # overwrite dl1 and start with ad part (positive)
        FOpers.ad(dl1,m,Kl1) # can't do ad in place, dummy
        # Flat back to momentum
        diffOp.ApplyOperator(dl1);
        # tack on already-computed ad^* part (negative)
        FOpers.Sub_I(dl1,scratchdl1)
        FOpers.Copy(dl1,scratchdl1)

        # Add l1 (positive since we computed negative l0 to start with)
        FOpers.Add_I(dl1,l0)

        # Update l1
        # scale l1 by dt for summing
        FOpers.Add_MulC_I(l1,dl1,-dt) # integrate l1

        # check if next step is a checkpoint
        if i-1 in checkpointinds:
            cp = checkpointstates[checkpointinds.index(i-1)]
            FOpers.Copy(g,cp[0])
            FOpers.Copy(ginv,cp[1])
        else:
            # TODO: Work out backward integration compatible with forward
            # i.e. do composeHV instead of composeVH here, etc.

            # otherwise integrate EPdiff to get to next time point
            FOpers.composeVH(g,m,g,-dt) # Take step for g and ginv
            # Initial estimate for ginv
            FOpers.composeHV(scratchdl1,ginv,m,dt)
            # Refine inverse
            #FOpers.FixedPointInverse(scratchdl1,g,Ninv)
            FOpers.Copy(ginv,scratchdl1)

def EvalForward(m0, # initial momentum
        t,diffOp,checkpointstates,checkpointinds,Ninv, # integration params
        I0, I1, # input images for computing residual image
        residimg, # residual image (output)
        scratch,integMethod='EULER'): # scratch vector field (for computing KNorm)
    """
    This computes pieces of the objective function, given initial momentum m0.
    This involves forward integration and saving all checkpoints, as well as
    computing the residual image, SSE and |m0|^2.  All these can be reused
    """

    # start by integrating forward
    integrateGeodesic(m0,t,diffOp,checkpointstates,checkpointinds,Ninv=Ninv,integMethod=integMethod)

    # get final state
    endidx = checkpointinds.index(len(t)-1)
    (g1,ginv1) = checkpointstates[endidx]

    # compute residual image
    IFOpers.applyH(residimg,I0,ginv1) # deform image
    #ITKFileIO.SaveImage("I01.mhd",residimg)
    IOpers.Sub_I(residimg,I1) # subtract to get image mismatch

    # compute image error
    SSE = Sum2(residimg)

    # compute norm of momentum
    FOpers.Copy(scratch,m0)
    diffOp.ApplyInverseOperator(scratch)
    KNorm = Dot(scratch,m0)

    # checkpointstates and residimg are updated in place
    # return everything that isn't updated in place
    return (SSE,KNorm)

if __name__ == '__main__':
    #mType = MEM_HOST
    mType = MEM_DEVICE

    prefix = 'testIndep2'
    
    if mType == MEM_HOST:
        prefix = prefix + 'HOST'
    else:
        prefix = prefix + 'GPU'

    # make output directory
    mkdir_p(prefix)

    # load base and target image
    I0 = common.LoadITKImage('testData/ImageSlice0.mhd',mType) # slices
    I1 = common.LoadITKImage('testData/ImageSlice4.mhd',mType)
    #I0 = common.LoadITKImage('testData/Image0.mhd',mType) # volumes
    #I1 = common.LoadITKImage('testData/Image4.mhd',mType)

    # circle to ellipse
    N = 128
    r = 32
    a = 50
    b = 20
    #a = 36
    #b = 36
    #I0 = Ellipse(N,r,r,mType)
    #I1 = Ellipse(N,a,b,mType)
    #ITKFileIO.SaveImage("I0ell.mhd",I0)
    #ITKFileIO.SaveImage("I1ell.mhd",I1)

    # set up time discretization
    Nt = 40
    t = [x*1./Nt for x in range(Nt)]
    Ninv = 0 # number of fixed point iterations to ensure a good inverse is computed

    # set up checkpointing for improved adjoint integration (you pay in memory)
    # It's probably a good idea to use 2 or 4 checkpoints at least..
    #checkpointinds = [] # no checkpointing
    checkpointinds = range(len(t)) # all checkpointing: no backward integration of g

    # set up scales
    # this should be a fine discretization when using interdependent scales
    #s = [1e5, 1e4, 1e3, 1e2, 1e1, 1]
    s = [1e5] # translation
    # start getting deformation around s=24000
    s = [1e4] # oversmooth
    s = [1e3] # pretty decent matching,maybe a little over smooth
    s = [1e2] # trouble converging but match looks good. ??? stepsize issue maybe
    s = [1e1] # trouble converging but match looks good. ???
    s = [2**(0.01*i) for i in range(1500,0,-1)] # bunch of scales!

    # set up regularization part
    a = [0.00001 for si in s] # E = a|m0|^2 + |I_0(1)-I_1|^2

    # Optimization parameters
    Niter = 200
    eps = 1000 # don't worry, maxpert takes care of this for the most part
    #optMethod = 'FIXEDGD'  # fixed stepsize gradient descent
    optMethod = 'ARMIJOGD' # gradient descent with backtracking line search
    #optMethod = 'NCG'      # non-linear conjugate gradient (with Armijo)
    #optMethod = 'LBFGS'    # l-BFGS quasi-Newton (with Armijo)

    (I0defVol,SSE,KNorm) = RunScaleWarpIndep(\
                            I0, \
                            I1, \
                            s, \
                            a, \
                            t, \
                            Ninv, \
                            checkpointinds, \
                            Niter, \
                            optMethod, \
                            eps, \
                            prefix)
                        
    ## plot SSE as a surface plot
    #fig = plt.figure(1)
    #plt.clf()
    #ax = fig.gca(projection='3d')
    ##S,N = np.meshgrid(range(Niter),scipy.log(s))
    #(S,N) = np.meshgrid(np.arange(Niter),np.arange(len(s)))
    #SSEa = np.array(SSE)
    #ax.plot_surface(S,N,SSEa)

    print "Saving volume of deformed images"

    # output warped image as 3D nrrd
    imSz = I0.Size()
    if (I0.Size().z == 1):
        IoutGrid = I0.grid() # copy original grid but add some depth
        IoutGrid.setSize(vec3di(imSz.x, imSz.y, len(s)))
        Is = Image3D(IoutGrid, MEM_HOST)
        Il = np.squeeze(I0defVol).tolist()
        # shuffle indices so that we have axial slices
        Il = np.transpose(Il,(1,2,0))
        # TODO: set up spacing and origin so that we overlay I0
        if len(s) == 1:
            Is.fromlist([Il])
        else:
            Is.fromlist(Il)
        ITKFileIO.SaveImage(prefix+"/I0defs.mhd",Is)
        # output diff of first with last
        Il -= I0.tolist()
        Is.fromlist(Il)
        ITKFileIO.SaveImage(prefix+"/I0diffs.mhd",Is)
    else:
        # Save out each volume individually
        for i in xrange(len(s)):
            ITKFileIO.SaveImage(prefix+"/I0defScale"+str(s[i])+".mhd",I0defVol[i])
