from PyCA.Core import *

import PyCA.Common as common
import PyCA.Display as display

import numpy as np
import matplotlib.pyplot as plt
import os, errno

import logging
import sys
import copy
import math
import time

def CheckNaN(l):
    if type(l) is list:
        x = sum(map(Sum, l))
    else:
        x = Sum(l)
    if math.isinf(x):
        raise Exception("\aInf detected")
    if x != x:
        raise Exception("\aNaN detected")

def adjpairs(l):
    """
    Given a list of size N, return a list of size N-1 containing all adjacent
    pairs of items

    Note that this makes a couple (shallow) copies of the list
    """
    return zip(l,l[1:])

class OptimizationConfig:
    def __init__(self, method='FIXEDGD', Niter=100, stepSize=[0.1], maxPert=None):
        self.method = method
        self.Niter = Niter
        self.stepSize = copy.copy(stepSize)
        self.maxPert = maxPert

class TimeDependentState:
    def __init__(self, t, grid, mType=MEM_HOST, k=1, Jt=None, checkpoint=False):
        self.t = t # time
        self.J = Jt # image data (if we have it here)

        if Jt is not None: # if we have image data here, we'll also need a residual image
            self.residimg = Image3D(grid, mType)
        else:
            self.residimg = None

        # set up checkpointing when requested, and force checkpointing at datapoints
        if checkpoint or Jt is not None:
            self.cpg = Field3D(grid, mType)
            self.cpginv = Field3D(grid, mType)
            self.cpm = [Field3D(grid, mType) for i in xrange(k)]
            SetToIdentity(self.cpg)
            SetToIdentity(self.cpginv)
            map(lambda _:SetMem(_,0), self.cpm)
        else:
            self.cpg = self.cgpinv = self.cpm = None

def SetupTimeDiscretization(t,J,Jind,checkpointinds,k=1,cpMemType=None):
    """
    Set up a tdisc object (list of TimeDependentStates)
    """
    grid = J[0].grid()
    if cpMemType is None:
        cpMemType = J[0].memType()
    td = []
    for i in xrange(len(t)):
        Jt = None
        if i in Jind:
            td.append(TimeDependentState(t[i],grid,cpMemType,k,J[Jind.index(i)],i in checkpointinds))
        else:
            td.append(TimeDependentState(t[i],grid,cpMemType,k,None,i in checkpointinds))
    return td

def BoundaryMask(grid,mType,n,blurSigma=0):
    I = np.zeros([grid.size().x,grid.size().y])

    for x in xrange(n,N-n):
        for y in xrange(n,N-n):
            I[x,y] = 1.0;

    I = common.GaussianBlur(I.T,blurSigma)

    return common.ImFromNPArr(I,mType=mType)

class State:
    def __init__(self, I0, alpha, beta, gamma, incomp,k=1,integMethod='EULER'):
        grid = I0.grid()
        mType = I0.memType()

        # initial conditions
        self.I0 = Image3D(grid,mType)
        Copy(self.I0, I0)
        self.m0 = [Field3D(grid,mType) for i in xrange(k)]

        # state at time t
        self.g = Field3D(grid,mType)
        self.ginv = Field3D(grid,mType)
        self.m = [Field3D(grid,mType) for i in xrange(k)]

        if integMethod == 'RK4': # need some extra memory for intermediates
            self.g1 = Field3D(grid,mType)
            self.ginv1 = Field3D(grid,mType)
            self.v1 = Field3D(grid,mType)
            self.dm1 = [Field3D(grid,mType) for i in xrange(k)]
            self.g2 = Field3D(grid,mType)
            self.ginv2 = Field3D(grid,mType)
            self.v2 = Field3D(grid,mType)
            self.dm2 = [Field3D(grid,mType) for i in xrange(k)]
            self.g3 = Field3D(grid,mType)
            self.ginv3 = Field3D(grid,mType)
            self.v3 = Field3D(grid,mType)
            self.dm3 = [Field3D(grid,mType) for i in xrange(k)]

        # sharped momentum
        self.v = [Field3D(grid,mType) for i in xrange(k)]

        # time deriv of momenta
        self.dm = [Field3D(grid,mType) for i in xrange(k)]

        # adjoint variables at time t
        self.l0 = Field3D(grid,mType)
        self.l = [Field3D(grid,mType) for i in xrange(k)]

        # sharped adjoint variables
        self.Kl = [Field3D(grid,mType) for i in xrange(k)]

        # time deriv of adjoint variables
        self.dl = [Field3D(grid,mType) for i in xrange(k)]

        # scratch
        self.scratchV = Field3D(grid, mType)

        self.l0scalar = Image3D(grid,mType) # the scalar part of l0
        self.l0scalartmp = Image3D(grid,mType) # temps for the sum of l0scalar

        #self.boundarymask = BoundaryMask(grid,mType,10,1)

        # differential operator (size same as a Field3D)
        if mType == MEM_HOST: self.diffOp = FluidKernelFFTCPU()
        else: self.diffOp = FluidKernelFFTGPU()
        self.diffOp.setAlpha(alpha)
        self.diffOp.setBeta(beta)
        self.diffOp.setGamma(gamma)
        self.diffOp.setDivergenceFree(incomp)
        self.diffOp.setGrid(grid)

def Step(p, dt):
    """
    Do a single Euler step
    """
    # Take step for g and ginv
    ComposeVH(p.scratchV,p.v[0],p.g,dt) # safe to use g for both args
    CheckNaN(p.scratchV)
    Copy(p.g,p.scratchV)
    CheckNaN(p.g)
    # Initial estimate for ginv
    CheckNaN(p.ginv)
    CheckNaN(p.v[0])
    ComposeHV(p.scratchV,p.ginv,p.v[0],-dt) # cannot use ginv for both args
    CheckNaN(p.scratchV)
    # Refine inverse using Ninv fixed point iterations
    #FixedPointInverse(scratchInv,g,Ninv)
    Copy(p.ginv,p.scratchV)
    CheckNaN(p.ginv)

    # we also need to update momenta
    map(lambda m,dm:Add_MulC_I(m,dm,dt), p.m, p.dm)
    if len(p.m) == 1: # closed form momentum update for geodesics
        CoAd(p.m[0], p.ginv, p.m0[0])
    CheckNaN(p.m)

def RK4Step(p, dt):
    """
    Do a single runge kutta step
    """
    ComputeVelocities(p) # initial smoothing
    # save initial state
    Copy(p.g1, p.g)
    Copy(p.ginv1, p.ginv)
    map(Copy, p.m1, p.m)
    # save Euler derivative
    ComputeMomentumDerivs(p)
    Copy(p.v1, p.v[0])
    map(Copy, p.dm1, p.dm)
    # take half step and recompute derivatives
    Step(p, 0.5*dt)
    ComputeVelocities(p)
    ComputeMomentumDerivs(p)
    # hold on to these new derivatives
    map(Copy, p.dm2, p.dm)
    Copy(p.v2, p.v[0])
    # reset
    Copy(p.g, p.g1)
    Copy(p.ginv, p.ginv1)
    map(Copy, p.m, p.m1)
    # take half step and recompute derivatives
    Step(p, 0.5*dt)
    ComputeVelocities(p)
    ComputeMomentumDerivs(p)
    # hold on to these new derivatives
    map(Copy, p.dm3, p.dm)
    Copy(p.v3, p.v[0])
    # reset
    Copy(p.g, p.g1)
    Copy(p.ginv, p.ginv1)
    map(Copy, p.m, p.m1)
    # take full step and recompute derivatives
    Step(p, dt)
    ComputeVelocities(p)
    ComputeMomentumDerivs(p)
    # reset
    Copy(p.g, p.g1)
    Copy(p.ginv, p.ginv1)
    map(Copy, p.m, p.m1)
    # combine everything to get final step direction
    Add_I(p.v[0], p.v1)
    Add_MulC_I(p.v[0], p.v2, 2.)
    Add_MulC_I(p.v[0], p.v3, 2.)
    DivC_I(p.v[0], 6.0)
    map(Add_I, p.dm, p.dm1)
    map(lambda a,b: Add_MulC_I(a,b,2.), p.dm, p.dm2)
    map(lambda a,b: Add_MulC_I(a,b,2.), p.dm, p.dm3)
    map(lambda a: DivC_I(a,6.), p.dm)
    # Finally, take a whole step in the final direction
    Step(p, dt)

def ComputeVelocities(p):
    """
    Computes the velocity by smoothing momentum
    """
    # Compute m_1 in closed form (at least first term)
    #CoAd(p.m[0], p.ginv, p.m0[0])
    map(p.diffOp.applyInverseOperator, p.v, p.m)
    #map(lambda _:Mul_I(_, p.boundarymask), p.v)

def ComputeMomentumDerivs(p):
    """
    Compute time derivatives of momentum vectors
    """
    # first velocity efficiently computed without diffOp'ing
    CheckNaN(p.m[0])
    CheckNaN(p.v[0])
    CoAdInf(p.dm[0], p.v[0], p.m[0])
    CheckNaN(p.dm[0])
    MulC_I(p.dm[0], -1.0)
    CheckNaN(p.dm[0])

    # for higher order velocities, we do the full rnabla
    for i in xrange(1,len(p.m)):
        Add_I(p.dm[i-1], p.m[i]) # polynomial chain part
        CheckNaN(p.dm[i-1])
        AdInf(p.dm[i], p.v[0], p.v[i]) # the rest is just barnabla
        CheckNaN(p.dm[i])
        p.diffOp.applyOperator(p.dm[i])
        CheckNaN(p.dm[i])
        CoAdInf(p.scratchV, p.v[0], p.m[i])
        CheckNaN(p.scratchV)
        Sub_I(p.dm[i], p.scratchV)
        CheckNaN(p.dm[i])
        CoAdInf(p.scratchV, p.v[i], p.m[0])
        CheckNaN(p.scratchV)
        Sub_I(p.dm[i], p.scratchV)
        CheckNaN(p.dm[i])
        MulC_I(p.dm[i], 0.5)
        CheckNaN(p.dm[i])

def SmoothAdjoints(p):
    map(p.diffOp.applyInverseOperator, p.Kl, p.l)
    #map(lambda _:Mul_I(_, p.boundarymask), p.Kl)

def Integrate(tdisc, p, integMethod='EULER'):
    """
    Integrate forward in order to fill out tdisc
    """
    # initial conditions 
    SetToIdentity(p.g)
    SetToIdentity(p.ginv)
    map(Copy, p.m, p.m0)
    CheckNaN(p.m0)
    CheckNaN(p.m)
    
    if True: # test zeroing out everything
        map(lambda _:SetMem(_,0),p.v)
        map(lambda _:SetMem(_,0),p.dm)
        SetMem(p.l0,0)
        map(lambda _:SetMem(_,0),p.l)
        map(lambda _:SetMem(_,0),p.Kl)
        map(lambda _:SetMem(_,0),p.dl)
        SetMem(p.scratchV,0)
        SetMem(p.l0scalar,0)
        SetMem(p.l0scalartmp,0)

    # do integration (Euler)
    for t,tnext in adjpairs(tdisc):
        #print('Time step %f -> %f' % (t.t, tnext.t))
        dt = tnext.t - t.t # time step
        if integMethod == "EULER":
            ComputeVelocities(p)
            CheckNaN(p.v)
            ComputeMomentumDerivs(p)
            CheckNaN(p.dm)

            Step(p, dt)
        elif integMethod == "RK4":
            RK4Step(p, dt)
        else:
            raise Exception('Unknown integration method: '+integMethod)

        # checkpointing
        if tnext.cpg is not None:
            Copy(tnext.cpg, p.g)
            Copy(tnext.cpginv, p.ginv)
            map(Copy, tnext.cpm, p.m)

def ComputeAdjointDerivs(p, tresids):
    """
    Compute the time derivatives of the adjoint variables.  l0 is computed in
    closed form here.  We assume that l1 and higher have been smoothed already,
    as has v
    """
    # dot l_1: this is -sym_v^*l = \ad_v Kl - \ad_{Kl}^*m
    # Smooth to get velocity at time t
    # overwrite dl1 and start with ad part (positive)
    AdInf(p.dl[0],p.v[0],p.Kl[0]) # can't do ad in place, dummy
    # Add the ad parts of higher terms influencing v_1
    for i in xrange(1,len(p.v)):
        AdInf(p.scratchV, p.v[i], p.Kl[i])
        Add_MulC_I(p.dl[0], p.scratchV, 0.5)
    p.diffOp.applyOperator(p.dl[0]) # Flat after we have all the ad()s
    CoAdInf(p.scratchV,p.Kl[0],p.m[0]) # coad part computed as scratchdl1
    Sub_I(p.dl[0],p.scratchV) # tack on ad^* part

    # compute l0 term
    # l0 is the diamond even if the original momentum wasn't a diamond
    SetMem(p.l0scalar,0.0)
    # check all future states where there's a residual
    for tfuture in tresids:
        ComposeHH(p.scratchV,p.g,tfuture.cpginv) # compute deformation to splat back with
        Splat(p.l0scalartmp,p.scratchV,tfuture.residimg,False) # splat
        Sub_I(p.l0scalar,p.l0scalartmp) # keep the sum
    # compute image I(t_i) and gradient
    ApplyH(p.l0scalartmp,p.I0,p.ginv)
    Gradient(p.l0,p.l0scalartmp)
    # multiply l0scalar by gradient of image at time t_i
    Mul_I(p.l0,p.l0scalar)

    # subtract l0
    Sub_I(p.dl[0],p.l0)

    # Add the coad parts of higher terms influencing v_1
    for i in xrange(1,len(p.m)):
        CoAdInf(p.scratchV, p.v[i], p.l[i])
        Add_MulC_I(p.dl[0], p.scratchV, 0.5)
        CoAdInf(p.scratchV, p.Kl[i], p.m[i])
        Add_MulC_I(p.dl[0], p.scratchV, -0.5)

    # now do the rest, these all look like parallel transport
    for i in xrange(1,len(p.m)):
        AdInf(p.dl[i], p.v[0], p.Kl[i])
        p.diffOp.applyOperator(p.dl[i])
        CoAdInf(p.scratchV, p.Kl[i], p.m[0])
        Sub_I(p.dl[i], p.scratchV)
        CoAdInf(p.scratchV, p.v[0], p.l[i])
        Sub_I(p.dl[i], p.scratchV)
        MulC_I(p.dl[i], 0.5)
        Sub_I(p.dl[i], p.l[i-1])

def AdjointIntegration(tdisc, p, integMethod='EULER'):
    if tdisc[-1].cpg is None:
        raise Exception('Endpoint checkpoint needs to be allocated during setup')

    # initialize state at endpoint
    Copy(p.g, tdisc[-1].cpg)
    Copy(p.ginv, tdisc[-1].cpginv)
    map(Copy, p.m, tdisc[-1].cpm)

    # we compute l0 on the fly, but we'll sum l1, so zero it first
    map(lambda _:SetMem(_,0), p.l)

    for (tprev,t) in reversed(adjpairs(tdisc)):
        dt = t.t - tprev.t # time step is positive
        
        # this compute m in closed form and smooths it to get v
        ComputeVelocities(p)
        # compute the derivative of the adjoint variable, given all future residuals
        SmoothAdjoints(p)
        ComputeAdjointDerivs(p, filter(lambda tf:tf.J is not None and tf.t>t.t, tdisc))

        # Update l1
        # scale l1 by dt for summing
        map(lambda l,dl: Add_MulC_I(l,dl,-dt), p.l, p.dl)

        # check if next step is a checkpoint
        if tprev.cpg is not None:
            Copy(p.g, tprev.cpg)
            Copy(p.ginv, tprev.cpginv)
            map(Copy, p.m, tprev.cpm)
        else:
            # TODO: Work out backward integration compatible with forward
            # i.e. do composeHV instead of composeVH here, etc.
            #ComputeMomentumDerivs(p)
            CheckNaN(p.ginv)
            CoAd(p.m[0], p.ginv, p.m0[0])
            CheckNaN(p.m[0])
            ComputeVelocities(p)
            CheckNaN(p.v[0])
            Step(p, -dt)

def EvalForward(tdisc, # integration params (includes data and residuals)
        p,integMethod='EULER', updateImage=True): # scratch vector field (for computing KNorm)
    """
    This computes pieces of the objective function, given initial momentum m0.
    This involves forward integration and saving all checkpoints, as well as
    computing the residual image, SSE and |m0|^2.  All these can be reused
    """

    # start by integrating forward
    Integrate(tdisc,p,integMethod=integMethod)

    if updateImage:
        #print('Updating base image')
        # recompute base image
        SetMem(p.I0, 1.0) # use this for splatting back ones
        #if tdisc[0].J is not None: # check first time (won't be checkpointed)
            #Copy(p.l0scalar, tdisc[0].J)
            #SetMem(p.l0scalartmp,1.0) # jacobian of identity
        #else:
        SetMem(p.l0scalar,0)
        SetMem(p.l0scalartmp,0)
        for t in filter(lambda _:_.J is not None, tdisc):
            Splat(t.residimg, t.cpginv, t.J, False) # splat
            CheckNaN(t.residimg)
            Add_I(p.l0scalar, t.residimg) # keep track of sums
            Splat(t.residimg, t.cpginv, p.I0, False) # splat
            Add_I(p.l0scalartmp, t.residimg) # keep track of sums
        CheckNaN(p.l0scalar)
        CheckNaN(p.l0scalartmp)
        CheckNaN(p.I0)
        Div(p.I0, p.l0scalar, p.l0scalartmp)

    SSE = 0.
    if tdisc[0].J is not None: # check first time (won't be checkpointed)
        Sub(tdisc[0].residimg, tdisc[0].J, p.I0)
        CheckNaN(tdisc[0].residimg)
        SSE += Sum2(tdisc[0].residimg)
    for t in filter(lambda _:_.J is not None, tdisc[1:]):
        # compute residual image and compute sum squared error
        ApplyH(t.residimg, p.I0, t.cpginv) # deform image
        CheckNaN(t.residimg)
        Sub_I(t.residimg, t.J) # subtract to get image mismatch
        MulC_I(t.residimg, -1.0)
        CheckNaN(t.residimg)
        SSE += Sum2(t.residimg) # compute image error

    # compute norm of momentum
    KNorm = 0.0
    for m0 in p.m0:
        p.diffOp.applyInverseOperator(p.scratchV, m0)
        KNorm += Dot(p.scratchV,m0)

    # checkpointstates and residimg are updated in place
    # return everything that isn't updated in place
    return (SSE,KNorm)

def PlotPolynomial(SSE, KNorm, tdisc, p, pause=True, outputPrefix=None):
    """
    Plot stuff like a slideshow of the data along with deforming base image
    """
    cpJ = filter(lambda t:t.J is not None, tdisc)

    if outputPrefix is not None:
        plt.figure('I0')
        plt.clf()
        display.DispImage(p.I0, newFig=False)
        plt.savefig(outputPrefix+"I0.pdf",transparent=True, bbox_inches='tight', pad_inches=0)
        plt.figure('energy')
        plt.clf()
        plt.plot(SSE,'k')
        plt.savefig(outputPrefix+"SSE.pdf",transparent=True, bbox_inches='tight', pad_inches=0)
        for i in xrange(len(cpJ)):
            plt.figure('J_'+str(i))
            display.DispImage(cpJ[i].J, title=None, newFig=False,t=True)
            plt.savefig(outputPrefix+"J"+str(i)+".pdf",transparent=True, bbox_inches='tight', pad_inches=0)
            ApplyH(p.l0scalartmp, p.I0, cpJ[i].cpginv)
            plt.figure('I0t_'+str(i))
            display.DispImage(p.l0scalartmp, newFig=False,t=True)
            plt.savefig(outputPrefix+"I0t"+str(i)+".pdf",transparent=True, bbox_inches='tight', pad_inches=0)
            plt.figure('phit_'+str(i))
            display.GridPlot(cpJ[i].cpg,color='k',plotBase=False, every=4)
            plt.axis('off')
            plt.axis('equal')
            plt.savefig(outputPrefix+"phit"+str(i)+".pdf",transparent=True, bbox_inches='tight', pad_inches=0)

    plt.figure('slideshow')
    plt.clf()
    for i in xrange(len(cpJ)):
        plt.subplot(3,len(cpJ),i+1)
        display.DispImage(cpJ[i].J, newFig=False,t=True)
        plt.title('J_'+str(i))
        plt.subplot(3,len(cpJ),i+1+len(cpJ))
        ApplyH(p.l0scalartmp, p.I0, cpJ[i].cpginv)
        display.DispImage(p.l0scalartmp, newFig=False,t=True)
        plt.title('I_0(t_'+str(i)+')')
        plt.subplot(3,len(cpJ),i+1+2*len(cpJ))
        display.GridPlot(cpJ[i].cpg,color='k',plotBase=False, every=4)
        plt.title('phi(t_'+str(i)+')')
        plt.xlabel('t = '+str(cpJ[i].t))

    plt.figure('I0')
    plt.clf()
    display.DispImage(p.I0, newFig=False)
    plt.title('I_0')

    plt.figure('energy')
    plt.clf()
    plt.plot(SSE,'r')
    #plt.hold(True)
    #plt.plot(KNorm,'b')

    # pause for user input
    if pause: raw_input("\aPress enter to continue...")

def Matching(momReg, tdisc, p, optim, integMethod='EULER', updateImage=True, plotEvery=1, outputPrefix=None):
    """
    Match two images using a geodesic and the direct Lie algebra Jacobi equation
    """
    if tdisc[-1].cpg is None:
        raise Exception("Time discretization must include checkpoint at end time")

    # Do first integration.  We integrate at the end of the loop
    (SSEi,KNormi) = EvalForward(tdisc,p,integMethod,updateImage=updateImage)

    # for smoothing momentum a little bit (HACK)
    if mType == MEM_HOST: m0smoother = FluidKernelFFTCPU()
    else: m0smoother = FluidKernelFFTGPU()
    m0smoother.setAlpha(10.)
    m0smoother.setBeta(0)
    m0smoother.setGamma(1.)
    m0smoother.setDivergenceFree(False)
    m0smoother.setGrid(p.I0.grid())

    
    # initialize error variables
    SSE = [SSEi]
    KNorm = [KNormi]

    for it in xrange(1,Niter+1):
        print "Iter %04d/%04d " % (it,Niter),

        # integrate adjoint system
        AdjointIntegration(tdisc,p,integMethod)
        CheckNaN(p.l)

        # avoid introducing crap near the boundaries
        #map(lambda _:Mul_I(_, p.boundarymask), p.l)

        map(lambda l: m0smoother.applyInverseOperator, p.l)
        CheckNaN(p.l)

        # add regularization term, gradient of penalty a|m_0|^2
        #Add_MulC_I(p.l[0],p.m0[0],momReg)
        map(lambda l,m0: Add_MulC_I(l,m0,-2.*momReg), p.l, p.m0)
        CheckNaN(p.l)

        # maxPert: to get step size or set reasonable first step size for adaptive
        if it == 1 and optim.maxPert is not None:
            # TODO: let maxPert be a list
            supnorml1 = LInf(p.l[0])
            if supnorml1*optim.stepSize > optim.maxPert:
                optim.stepSize = optim.maxPert/supnorml1

        # take gradient descent step
        if optim.method == 'FIXEDGD':
            # Go ahead and update momentum
            map(Add_MulC_I,p.m0,p.l,optim.stepSize[0:len(p.m0)])
            CheckNaN(p.m0)

            # Integrate forward and report error
            (SSEi,KNormi) = EvalForward(tdisc,p,integMethod,updateImage=updateImage)
        elif optim.method == 'ARMIJOGD':
            # Perform backtracking (and expansive) line search for epsilon
            # The line search is an important piece of both NCG and LBFGS
            # methods as well.  Both of those should provide faster convergence
            # so this method is more of a testbed.

            # NOTE: this calls EvalForward
            if it < 10:
                (eps,SSEi,KNormi) = ArmijoLineSearch(l1,eps,SSEi+a*KNormi,a,t,checkpointstates,checkpointinds,Ninv,I0,I1,residimg,scratch,l0,integMethod) # reuse l0
            else:
                Add_MulC_I(p.m0[0],l1,-eps)
                (SSEi,KNormi) = EvalForward(t,checkpointstates,checkpointinds,Ninv,residimg,l0,integMethod)

        elif optim.method == 'NCG':
            raise Exception('Non-linear conjugate gradient not yet implemented')
        elif optim.method == 'LBFGS':
            # For this method we approximate the inverse hessian using a lot of
            # rank-one updates.  For the rank-one matrices we actually just save
            # the gradients themselves.  This is the difference between l-BFGS
            # and regular BFGS, which is infeasible in this case because of the
            # huge problem size.  We reset whenever the inner product between
            # the new search direction and the gradient is less than .5 (angle
            # greater than 60 degrees)
            raise Exception('l-BFGS quasi-Newton not yet implemented')
        else:
            raise Exception('Unknown optimization method: '+optim.method)

        # report error after this step
        print "momReg=%f SSE=%f |m0|^2=%f E=%f" % (momReg,SSEi,KNormi,SSEi+momReg*KNormi)

        # save error
        SSE.append(SSEi)
        KNorm.append(KNormi)

        problemiter=10000

        if it == problemiter:
            raw_input("\aShit's about to get real. Press enter and watch the residual")

        if it > problemiter:
            plt.figure('quiver')
            display.Quiver(p.m0[0])
            plt.savefig('quiver'+str(it)+'.png')
            plt.figure('mag')
            SqrMagnitude(p.l0scalartmp,p.m0[0])
            display.DispImage(p.l0scalartmp,newFig=0,rng=[0,1e4])
            plt.savefig('mag'+str(it)+'.png')
            plt.figure('v0')
            p.diffOp.applyInverseOperator(p.v[0],p.m0[0])
            display.Quiver(p.v[0])
            plt.savefig('v0'+str(it)+'.png')


        if plotEvery > 0 and it%plotEvery == 0 or it == Niter-1:
            PlotPolynomial(SSE, map(lambda _:_*momReg, KNorm), tdisc, p, pause=True, outputPrefix=outputPrefix)

        # display the residual image
        #dispImage(residimg,sliceIdx=0)

        # convergence criterion
        #if iter > 30 and abs(SSEi-SSE[-2]) < SSE[0]*1e-8:
            #break

    return (SSE,KNorm)

# safe, silent mkdir
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST:
            pass
        else: raise

def Ellipse(N,a,b,mType,blurSigma=0):
    I = np.zeros([N,N])

    for x in xrange(N):
        for y in xrange(N):
            xw = x - N/2.
            yw = y - N/2.
            X = xw/a
            Y = yw/b
            if (X*X + Y*Y) < 1.:
                I[x,y] = 1.

    I = common.GaussianBlur(I.T,blurSigma)

    return common.ImFromNPArr(I,mType=mType)

def makeC(N,r1,r2,t1,t2,blurSigma=0.):
    # we'll populate a numpy array first
    Il = np.zeros([N,N])

    for x in xrange(N):
        for y in xrange(N):
            xw = (x - N/2.)/float(N)
            yw = (y - N/2.)/float(N)
            # get radius
            r = math.sqrt(xw*xw+yw*yw)
            # get theta
            t = math.atan2(yw,-xw)
            if (t1 <= t <= t2) and (r1 <= r <= r2):
                Il[x,y] = 1.

    # blur
    Il = common.GaussianBlur(Il.T,blurSigma)

    # convert to Image3D
    return common.ImFromNPArr(Il,mType=mType)


    # Upload to device if requested
    I.toType(mType)

if __name__ == '__main__':
    #mType = MEM_HOST
    mType = MEM_DEVICE

    prefix = 'testIndep2'
    
    if mType == MEM_HOST: prefix = prefix + 'HOST'
    else: prefix = prefix + 'GPU'

    # make output directory
    mkdir_p(prefix)

    ## load base and target image
    #I0l = common.LoadITKImage('testData/ImageSlice0.mhd',mType) # slices
    #I2l = common.LoadITKImage('testData/ImageSlice2.mhd',mType)
    #I4l = common.LoadITKImage('testData/ImageSlice4.mhd',mType)
    ##I0 = common.LoadITKImage('testData/Image0.mhd',mType) # volumes
    ##I1 = common.LoadITKImage('testData/Image4.mhd',mType)
    #J = [I0l, I2l, I4l]
    #Jind = [0,19,39]

    # set up time discretization
    Nt = 100
    t = [(x)*1./(Nt-1.) for x in range(Nt)]
    #Ninv = 0 # number of fixed point iterations to ensure a good inverse is computed

    # circle to ellipse
    N = 128
    ds = 0.8
    r = N*.2*ds
    a = N*.3*ds
    b = N*.2*ds
    c = N*.37*ds
    d = N*.15*ds
    #a = 36
    #b = 36
    imageSigma = N/38
    I0l = Ellipse(N,r,r,mType,imageSigma)
    I1l = Ellipse(N,a,b,mType,imageSigma)
    I2l = Ellipse(N,c,d,mType,imageSigma)
    #J = [I0l, I1l, I2l]
    #Jt = [0.0,.85,1.0]
    J=[I0l,I1l,I2l]
    Jt =[0,0.5,1.]
    
    ## C data
    ## size of image and C
    #N = 256
    #r1 = .125
    #r2 = .3
    #t1 = -math.pi*.75  # start angle
    #t2 = 0             # end angle for I0
    #t3 = math.pi*.5   # end angle for I1
    ## sigma for smoothing images
    #imageSigma = N/32.
    #Nd = 2 # number of data points
    #Jt = [(i)/(Nd-1.) for i in xrange(Nd)]
    ## Generate data
    #J = [makeC(N,r1,r2,t1,t2+(t3-t2)*tt,blurSigma=imageSigma) for tt in Jt]

    t = sorted(list(set(t+Jt)))
    Jind = map(t.index, Jt)

    # set up checkpointing for improved adjoint integration (you pay in memory)
    # It's probably a good idea to use 2 or 4 checkpoints at least..
    #checkpointinds = [] # no checkpointing except at datapoints
    checkpointinds = range(len(t)) # all checkpointing: no backward integration of g
    cpMemType = None # use same type as state TODO: fix streaming
    #cpMemType = MEM_HOST

    alpha = 1e2
    beta = 0
    gamma = 1
    incomp = False

    k=1 # polynomial order

    maxPert = None # no maxPert
    #maxPert = 1*gamma

    # set up regularization part
    momReg = 0.00005

    # Optimization parameters
    Niter = 2000
    # stepsizes for velocity, acceleration, jerk, snap, crackle, pop, ...
    eps = [20., 50.0]
    optMethod = 'FIXEDGD'  # fixed stepsize gradient descent
    #optMethod = 'ARMIJOGD' # gradient descent with backtracking line search
    #optMethod = 'NCG'      # non-linear conjugate gradient (with Armijo)
    #optMethod = 'LBFGS'    # l-BFGS quasi-Newton (with Armijo)

    #integMethod = 'EULER'
    integMethod = 'RK4'

    optcfg = OptimizationConfig(method=optMethod, Niter=Niter, stepSize=eps, maxPert=maxPert)

    m0all = []
    I0all = []
    for ki in xrange(1,k+1):
        # start with geodesic regression and go up from there to final k

        p = State(J[0], alpha,beta,gamma,incomp, ki, integMethod=integMethod)
        tdisc = SetupTimeDiscretization(t,J,Jind,checkpointinds, ki, cpMemType=cpMemType)
        map(lambda _:SetMem(_,0), p.m0)
        if ki==1: # initial momentum (output)
            # initial image
            SetMem(p.I0, 0)
            for Ji in J: Add_I(p.I0,Ji)
            DivC_I(p.I0, N)
            pass
        else:
            map(Copy, p.m0[0:ki-1], m0all[ki-2])
            Copy(p.I0, I0all[ki-2])

        (SSEi,KNormi) = Matching(momReg,tdisc,p,optcfg,updateImage=True,plotEvery=5000,outputPrefix='ImageRegressionK'+str(ki))
        
        # hold on to all the initial momenta and base images
        m0i = [Field3D(J[0].grid(), MEM_HOST) for i in xrange(ki)]
        I0i = Image3D(J[0].grid(), MEM_HOST)
        map(Copy, m0i, p.m0)
        Copy(I0i, p.I0)
        m0all.append(m0i)
        I0all.append(I0i)
